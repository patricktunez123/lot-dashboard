import React from "react";
import { Input } from "antd";
import { Link, useHistory } from "react-router-dom";
import "./Navbar.scss";
import logout_black from "../../images/logout_black.svg";
import moon from "../../images/moon.svg";
import { routes } from "../../config/route-config";
import { handleTitle } from "../../helpers/handleTitle";

const { Search } = Input;

const Navbar = () => {
  const onSearch = (value) => console.log(value);
  const history = useHistory();

  const handleLogout = () => {
    localStorage.removeItem("lotUserToken");
    localStorage.removeItem("lotUserData");
    history.push(routes.login.url);
  };

  return (
    <div className="lot_navbar">
      <h6 className="lot_medium_title lot_primary_text lot_semiBold_text">
        {history.location.pathname && history.location.pathname
          ? handleTitle(history.location.pathname)
          : "LoT Dashboard"}
      </h6>
      <Search
        className="lot_search_input lot_input"
        placeholder="Search for patients"
        onSearch={onSearch}
      />
      <div className="lot_navbar_btns">
        <Link to={routes.overview.url}>
          <img src={moon} alt="" />
        </Link>

        <img
          onClick={handleLogout}
          className="lot_cursored_text"
          src={logout_black}
          alt=""
        />
      </div>
    </div>
  );
};

export default Navbar;
