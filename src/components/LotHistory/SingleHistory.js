import React from "react";
import moment from "moment";
import people from "../../images/history/people.svg";
import menu from "../../images/history/menu.svg";
import apartment from "../../images/history/apartment.svg";

const History = ({ title, start, end }) => {
  return (
    <div className="lot_history">
      {title && title === "Legacy parse" ? (
        <img src={people} alt="" />
      ) : (
        <img src={apartment} alt="" />
      )}
      <div>
        <p className="lot_extra_small_text">{title}</p>
        <p className="lot_extra_small_text lot_muted_text">
          {moment(start).format("LT")} - {moment(end).format("LT")}
        </p>
      </div>
      <img src={menu} alt="" />
    </div>
  );
};

export default History;
