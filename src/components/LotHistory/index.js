import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { MdNavigateNext } from "react-icons/md";
import { GrFormPrevious } from "react-icons/gr";
import "./LotHistory.scss";
import SingleHistory from "./SingleHistory";
import { getAllHistories } from "../../redux/actions/history.actions";
import { HistoriesLoader } from "../Loaders";

const LotHistory = () => {
  const dispatch = useDispatch();
  const { data: histories, loading } = useSelector((state) => state.Histories);

  useEffect(() => {
    dispatch(getAllHistories());
  }, [dispatch]);

  return (
    <div className="lot_histories">
      <div className="lot_margin_1">
        <div className="header">
          <Link to="/overview">
            <GrFormPrevious />
          </Link>
          <span className="lot_small_text">Today</span>
          {/* <span className="lot_small_text">Today, 10 June</span> */}
          <Link to="/overview">
            <MdNavigateNext />
          </Link>
        </div>
        <div className="lot_histories_container lot_margin_2_top">
          {loading ? (
            <HistoriesLoader />
          ) : (
            histories?.map((history) => (
              <SingleHistory key={history?.id} {...history} />
            ))
          )}
        </div>
      </div>
    </div>
  );
};

export default LotHistory;
