import React from "react";
import { HistoriesLoader } from "../Loaders";

export const EmployeesOverViewCard = ({ countLoading, count }) => {
  return (
    <>
      {countLoading ? (
        <div className="overview_card">
          <h6 className="lot_white_text lot_semiBold_text lot_uppercased_text">
            Employers
          </h6>
          <HistoriesLoader />
        </div>
      ) : (
        <div className="overview_card">
          <h6 className="lot_white_text lot_semiBold_text lot_uppercased_text">
            Employees
          </h6>
          <p className="lot_big_text lot_cyan_text">{count}</p>
          <p className="lot_white_text lot_extra_small_text">
            {count} employees, Employers finds thier employess through us
          </p>
        </div>
      )}
    </>
  );
};
