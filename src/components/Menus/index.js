import { AiFillHome, AiTwotoneCalendar } from "react-icons/ai";
import { FaUserAlt } from "react-icons/fa";
import { RiArticleLine } from "react-icons/ri";
import { MdSettings } from "react-icons/md";
import { routes } from "../../config/route-config";

export const webMenus = [
  {
    id: 1,
    title: "Home",
    icon: <AiFillHome />,
    url: routes.overview.url,
  },
  {
    id: 2,
    title: "Appointments",
    icon: <AiTwotoneCalendar />,
    url: routes.appointments.url,
  },
  {
    id: 3,
    title: "Staffs",
    icon: <FaUserAlt />,
    url: routes.staffs.url,
  },
  {
    id: 4,
    title: "Reports",
    icon: <RiArticleLine />,
    url: routes.reports.url,
  },
  {
    id: 5,
    title: "Settings",
    icon: <MdSettings />,
    url: routes.settings.url,
  },
];
