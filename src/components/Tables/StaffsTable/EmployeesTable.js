import React, { useEffect } from "react";
import { Table, Popconfirm, message } from "antd";
import { MdEdit, MdDelete } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllEmployees,
  deleteEmployee,
} from "../../../redux/actions/employees.actions";
import "../../../styles/tables.scss";
import { TableLoader } from "../../Loaders";
import no_image from "../../../images/staffs/no_image.png";

export const EmployeesTable = () => {
  const dispatch = useDispatch();
  const { loading, data } = useSelector((state) => state.Employees);
  const {
    loading: deleteLoading,
    data: deleteData,
    errorMessage,
  } = useSelector((state) => state.DeleteEmployee);

  const confirm = (id) => {
    dispatch(deleteEmployee(id));
    message.success(deleteData?.data?.message);
  };
  errorMessage && message.success(errorMessage);
  const cancel = (id) => {
    message.info("Delete req canceled");
  };

  useEffect(() => {
    dispatch(getAllEmployees());
  }, [dispatch]);

  const columns = [
    {
      dataIndex: "picture",
      key: "picture",
      render: (_, record) => {
        return (
          <div className="lot_multiple_table_item">
            <div className="staff_info">
              <img src={record?.picture ? record?.picture : no_image} alt="" />
              <p className="lot_small_text lot_semiBold_text">
                {record?.firstName} {record?.lastName}
              </p>
            </div>
          </div>
        );
      },
    },
    {
      dataIndex: "company",
      key: "company",
      render: (_, record) => {
        return (
          <div className="lot_multiple_table_item">
            <div className="staff_info">
              <p className="lot_small_text lot_semiBold_text">
                {record?.company ? record?.company : "No company"}
              </p>
            </div>
          </div>
        );
      },
    },

    {
      render: (_, record) => {
        return (
          <div className="lot_action_buttons_group">
            <MdEdit className="lot_muted_text2 lot_cursored_text text_16" />
            <Popconfirm
              title="Are you sure to delete this?"
              onConfirm={() => confirm(record?.id)}
              onCancel={() => cancel(record?.id)}
              okText="Yes, Delete"
              cancelText="No, Cancel"
            >
              <MdDelete className="lot_muted_text2 lot_cursored_text text_16" />
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  return (
    <div>
      {loading ? (
        <TableLoader />
      ) : (
        <Table
          className="table-striped-rows lot_custom_table"
          columns={columns}
          dataSource={data}
          pagination={true}
          loading={deleteLoading}
        />
      )}
    </div>
  );
};
