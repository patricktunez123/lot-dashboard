import React, { useState } from "react";
import { Table, Rate, Modal, Button, Popconfirm, message } from "antd";
import { MdEdit, MdDelete } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteCompany,
  getCompany,
} from "../../../redux/actions/staff.actions";
import "../../../styles/tables.scss";
import { TableLoader } from "../../Loaders";
import no_image from "../../../images/staffs/no_image.png";
import { RateForm } from "../../Forms";

export const StaffsTable = ({ loading, staffs, countLoading, count }) => {
  const dispatch = useDispatch();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const {
    loading: deleteLoading,
    deleteData,
    errorMessage,
  } = useSelector((state) => state.DeleteCompany);

  const {
    loading: companyLoading,
    data: companyData,
    errorMessage: companyErrorMessage,
  } = useSelector((state) => state.Company);

  const showModal = (id) => {
    setIsModalVisible(true);
    dispatch(getCompany(id));
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const confirm = (id) => {
    dispatch(deleteCompany(id));
    message.success(deleteData?.data?.message);
  };

  errorMessage && message.success(errorMessage);

  const cancel = () => {
    message.info("Delete req canceled");
  };

  const columns = [
    {
      dataIndex: "picture",
      key: "picture",
      render: (_, record) => {
        return (
          <div className="lot_multiple_table_item">
            <div className="staff_info">
              <img src={record?.picture ? record?.picture : no_image} alt="" />
              <p className="lot_small_text lot_semiBold_text">{record?.name}</p>
            </div>
          </div>
        );
      },
    },
    {
      dataIndex: "rating",
      key: "rating",
      render: (_, record) => {
        return (
          <div className="lot_multiple_table_item">
            <Rate disabled defaultValue={record?.rating} />
          </div>
        );
      },
    },
    {
      render: (_, record) => {
        return (
          <div className="lot_multiple_table_item">
            <Button
              onClick={() => showModal(record?.id)}
              className="lot_link_btn blue_btn lot_semiBold_text"
            >
              Rate
            </Button>
            <Modal
              title="Rate company"
              visible={isModalVisible}
              onOk={handleOk}
              onCancel={handleCancel}
              footer={false}
              closable={false}
              className="lot_modal"
              mask={false}
            >
              <RateForm
                companyLoading={companyLoading}
                companyData={companyData}
                companyErrorMessage={companyErrorMessage}
                setIsModalVisible={setIsModalVisible}
              />
            </Modal>
          </div>
        );
      },
    },
    {
      render: (_, record) => {
        return (
          <div className="lot_action_buttons_group">
            <MdEdit className="lot_muted_text2 lot_cursored_text text_16" />
            <Popconfirm
              title="Are you sure to delete this company?"
              onConfirm={() => confirm(record?.id)}
              onCancel={() => cancel(record?.id)}
              okText="Yes, Delete"
              cancelText="No, Cancel"
            >
              <MdDelete className="lot_muted_text2 lot_cursored_text text_16" />
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  return (
    <div>
      {loading ? (
        <TableLoader />
      ) : (
        <Table
          className="table-striped-rows lot_custom_table"
          columns={columns}
          dataSource={staffs}
          pagination={true}
          loading={deleteLoading}
        />
      )}
    </div>
  );
};
