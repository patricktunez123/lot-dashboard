import React from "react";
import { Table } from "antd";
import { AiTwotoneCompass } from "react-icons/ai";
import { TableLoader } from "../../Loaders";

export const ReportsTable = ({ loading, reports }) => {
  console.log("sss", reports);
  const columns = [
    {
      title: (
        <span className="lot_table_title lot_text_14 lot_semiBold_text">
          STUDENTS
        </span>
      ),
      key: "studentName",
      dataIndex: "studentName",
      render: (_, record) => {
        return (
          <div className="lot_multiple_table_item">
            <span className="lot_text_14 lot_black_text">{record?.key}</span>
            <div className="lot_flex">
              <span className="lot_text_14 lot_blue_text2">
                {record?.studentName}
              </span>
              <span className="lot_text_12 lot_muted_text">
                {record?.studentEmail}
              </span>
            </div>
          </div>
        );
      },
    },
    {
      title: (
        <span className="lot_table_title lot_text_14 lot_semiBold_text">
          COMPANIES
        </span>
      ),
      dataIndex: "company",
      key: "company",
      render: (_, record) => {
        return (
          <div className="lot_multiple_table_item">
            <span className="lot_text_14">
              <AiTwotoneCompass />
            </span>
            <span className="lot_text_14 lot_blue_text2">
              {record?.company}
            </span>
          </div>
        );
      },
    },
    {
      title: (
        <span className="lot_table_title lot_text_14 lot_semiBold_text">
          DATE JOINED
        </span>
      ),
      dataIndex: "dateJoined",
      key: "dateJoined",
      render: (_, record) => {
        return (
          <div className="lot_multiple_table_item">
            <span className="lot_text_14 lot_muted_text">
              {record?.dateJoined}
            </span>
          </div>
        );
      },
    },
    {
      title: (
        <span className="lot_table_title lot_text_14 lot_semiBold_text">
          SERVICE FEE
        </span>
      ),
      dataIndex: "serviceFee",
      key: "serviceFee",
      render: (_, record) => {
        return (
          <div className="lot_multiple_table_item">
            <span className="lot_text_14 lot_primary_text lot_uppercased_text lot_semiBold_text">
              {record?.serviceFee}
            </span>
          </div>
        );
      },
    },
  ];

  return (
    <div>
      {loading ? (
        <TableLoader />
      ) : (
        <Table
          className="table-striped-rows"
          columns={columns}
          dataSource={reports}
          pagination={false}
        />
      )}
    </div>
  );
};
