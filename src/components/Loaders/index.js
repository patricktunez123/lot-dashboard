import React from "react";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import "./Loaders.scss";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

export const HistoriesLoader = () => {
  return (
    <div className="lot_loader_container">
      <Spin indicator={antIcon} />
    </div>
  );
};

export const TableLoader = () => {
  return (
    <>
      {[...Array(7)].map((_, index) => (
        <div key={index} className="col-lg-12 col-md-12 col-12">
          <SkeletonTheme color="#fff" highlightColor="#f7fafd">
            <Skeleton width="100%" height="4rem" />
          </SkeletonTheme>
        </div>
      ))}
    </>
  );
};
