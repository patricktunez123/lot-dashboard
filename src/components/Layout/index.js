import React from "react";
import "./Layout.scss";
import Navbar from "../Navbar";
import Sidebar from "../Sidebar";
// import ToggledSideBar from "../Sidebar/ToggledSideBar";
import LotHistory from "../LotHistory";

const Layout = ({ children }) => {
  return (
    <>
      <div className="lot_dashboard">
        <Sidebar />
        {/* <ToggledSideBar /> */}
        <div className="lot_dashboard_app_container">
          <div className="lot_header">
            <Navbar />
          </div>

          <div className="lot_dashboard_body">
            <div className="lot_dashboard_pages">{children}</div>
            <LotHistory />
          </div>
        </div>
      </div>
    </>
  );
};

export default Layout;
