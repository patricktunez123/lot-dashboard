import * as Yup from "yup";
import { name, email, password } from "../../helpers/InputValidation";

export const initialValues = {
  name,
  email,
  password,
};

export const validationSchema = Yup.object().shape({
  name,
  email,
  password,
});
