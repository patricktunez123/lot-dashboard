import React, { useEffect, useState } from "react";
import { Rate, Button, message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { rateCompany } from "../../redux/actions/staff.actions";
import no_image from "../../images/staffs/no_image.png";
import "./Form.scss";
import { HistoriesLoader } from "../Loaders";

export const RateForm = ({
  companyLoading,
  companyData,
  setIsModalVisible,
}) => {
  const dispatch = useDispatch();
  const [userRate, setUserRate] = useState(companyData?.data?.payload?.rating);
  const { loading, errorMessage, data } = useSelector(
    (state) => state.RateCompany
  );
  const onChange = (value) => {
    setUserRate(value);
  };

  const handleRating = () => {
    dispatch(rateCompany(companyData?.data?.payload?.id, userRate));
    // !loading && setIsModalVisible(false);
  };

  useEffect(() => {
    if (data?.data?.message) {
      message.info(data?.data?.message);
    } else if (errorMessage?.message) {
      message.error(errorMessage?.message);
    }
  }, [data?.data?.message, errorMessage?.message]);

  return (
    <>
      {companyLoading ? (
        <div className="lot_margin_1_top">
          <div className="lot_form">
            <div className="campany_logo">Wait...</div>
            <p className="lot_margin_1_bottom text-center lot_muted_text lot_extra_small_text">
              <HistoriesLoader />
            </p>
          </div>
        </div>
      ) : (
        <div className="lot_margin_1_top">
          <div className="lot_form">
            <div className="campany_logo">
              <img
                className="company_img"
                src={
                  companyData?.data?.payload?.picture
                    ? companyData?.data?.payload?.picture
                    : no_image
                }
                alt=""
              />
            </div>
            <h6 className="lot_margin_2_top lot_margin_1_bottom lot_primary_text lot_semiBold_text lot_medium_title">
              {companyData?.data?.payload?.name}
            </h6>
            <p className="lot_margin_1_bottom text-center lot_muted_text lot_extra_small_text">
              To rate this company you will First choose the stars and save By
              clicking “rate”
            </p>
            <div className="lot_margin_1_bottom ratings">
              <h6 className="lot_semiBold_text lot_small_text">Rate</h6>
              <Rate
                onChange={onChange}
                defaultValue={companyData?.data?.payload?.rating}
              />
            </div>
            {loading ? (
              <Button
                disabled
                className="lot_margin_1_top lot_btn lot_large_btn lot_btn_primary_btn"
                type="primary"
                htmlType="submit"
              >
                Rating...
              </Button>
            ) : (
              <Button
                className="lot_margin_1_top lot_btn lot_large_btn lot_btn_primary_btn"
                type="primary"
                htmlType="submit"
                onClick={handleRating}
              >
                Rate
              </Button>
            )}
          </div>
        </div>
      )}
    </>
  );
};
