import React, { useEffect } from "react";
import { Form, Input, Button, message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import { routes } from "../../config/route-config";
import { login } from "../../redux/actions/auth.actions";
import "./Form.scss";

export const LoginForm = () => {
  const dispatch = useDispatch();
  let { loading, errorMessage, successMessage } = useSelector(
    (state) => state.Login
  );

  console.log("====>", errorMessage, successMessage);
  const onFinish = (values) => {
    dispatch(login(values));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  useEffect(() => {
    if (loading) {
      message.loading("LoT's connecting, pls wait..");
    } else {
      if (errorMessage) {
        message.error("Check your credetials and try again");
      } else if (successMessage) {
        message.success(successMessage);
      }
    }
  }, [loading, errorMessage, successMessage]);

  const userToken = localStorage.getItem("lotUserToken");
  if (userToken) return <Redirect to={routes.overview.url} />;

  return (
    <div className="lot_margin_1_top">
      <Form
        name="loginForm"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        layout="vertical"
      >
        <Form.Item
          label="Email*"
          name="email"
          rules={[
            {
              type: "email",
              required: true,
              message: "Your valid email",
            },
          ]}
        >
          <Input
            className="lot_input lot_bordered_input"
            placeholder="Kamikazililiane@gmail.com"
          />
        </Form.Item>

        <Form.Item
          label="Enter Password*"
          name="password"
          rules={[
            {
              required: true,
              message: "Your password",
            },
          ]}
        >
          <Input.Password
            placeholder="****************************"
            iconRender={(visible) => (visible ? "Hide" : "Show")}
            className="lot_input"
          />
        </Form.Item>

        <Link to="/" className="lot_red_text">
          Forgot Password?
        </Link>

        <Form.Item>
          {loading ? (
            <Button
              disabled
              className="lot_margin_2_top lot_btn lot_large_btn lot_btn_primary_btn"
              type="primary"
              htmlType="submit"
              loading={loading}
            >
              Wait...
            </Button>
          ) : (
            <Button
              className="lot_margin_2_top lot_btn lot_large_btn lot_btn_primary_btn"
              type="primary"
              htmlType="submit"
            >
              Login
            </Button>
          )}
        </Form.Item>
      </Form>

      <p className="lot_margin_3_top lot_small_text lot_muted_text text-center">
        Don’t Have An Account?{" "}
        <Link className="lot_primary_text" to={routes.register.url}>
          Signup
        </Link>
      </p>
    </div>
  );
};
