import React, { useEffect } from "react";
import { Button, Checkbox, Divider, Form, Input, message } from "antd";
import { Link, Redirect } from "react-router-dom";
import { FcGoogle } from "react-icons/fc";
import { useDispatch, useSelector } from "react-redux";
import {
  userRegister,
  registerWithGoogle,
} from "../../redux/actions/auth.actions";
import { routes } from "../../config/route-config";
import "./Form.scss";

export const RegisterForm = () => {
  const dispatch = useDispatch();
  const { loading, errorMessage, successMessage } = useSelector(
    (state) => state.UserRegister
  );
  const onFinish = (values) => {
    dispatch(userRegister(values));
  };

  const handleCreateAccountWithGoogle = () => {
    dispatch(registerWithGoogle());
  };
  useEffect(() => {
    if (loading) {
      message.loading("LoT's connecting, pls wait..");
    } else {
      if (errorMessage) {
        message.error(errorMessage);
      } else if (successMessage) {
        message.success(successMessage);
      }
    }
  }, [loading, errorMessage, successMessage]);

  const userToken = localStorage.getItem("lotUserToken");
  if (userToken) return <Redirect to={routes.overview.url} />;

  return (
    <div className="lot_margin_1_top">
      <Form onFinish={onFinish} id="register" layout="vertical">
        <Form.Item
          label="Full Name*"
          name="name"
          rules={[
            {
              required: true,
              message: "Your name is needed",
            },
          ]}
        >
          <Input
            className="lot_input lot_bordered_input"
            placeholder="Kamikazi Liliane"
            name="name"
          />
        </Form.Item>

        <Form.Item
          label="Email*"
          name="email"
          rules={[
            {
              type: "email",
              required: true,
              message: "Your valid email",
            },
          ]}
        >
          <Input
            className="lot_input lot_bordered_input"
            placeholder="Kamikazililiane@gmail.com"
            name="email"
          />
        </Form.Item>

        <Form.Item
          label="Enter Password*"
          name="password"
          rules={[
            {
              required: true,
              message: "Enter your password",
            },
          ]}
        >
          <Input.Password
            placeholder="****************************"
            iconRender={(visible) => (visible ? "Hide" : "Show")}
            className="lot_input"
            name="password"
          />
        </Form.Item>

        <Form.Item name="agree" valuePropName="checked">
          <Checkbox>I agree to terms and Conditions</Checkbox>
        </Form.Item>
        {loading ? (
          <Button
            disabled
            className="lot_btn lot_large_btn lot_btn_primary_btn"
            type="primary"
            htmlType="submit"
            loading={loading}
          >
            Wait...
          </Button>
        ) : (
          <Button
            className="lot_btn lot_large_btn lot_btn_primary_btn"
            type="primary"
            htmlType="submit"
          >
            Create Account
          </Button>
        )}
      </Form>

      <p className="text-center lot_muted_text lot_margin_1_bottom">
        <Divider>
          <span className="lot_muted_text lot_text_14">Or</span>
        </Divider>
      </p>

      <div className="signup_with_google lot_margin_1_bottom">
        <p
          onClick={handleCreateAccountWithGoogle}
          className="lot_cursored_text"
        >
          <FcGoogle />
        </p>
        <p
          onClick={handleCreateAccountWithGoogle}
          className="lot_small_text lot_black_text lot_cursored_text"
        >
          Sign Up With Google
        </p>
      </div>

      <p className="lot_small_text lot_muted_text text-center">
        Already Have An Account?{" "}
        <Link className="lot_primary_text" to={routes.login.url}>
          Signin
        </Link>
      </p>
    </div>
  );
};
