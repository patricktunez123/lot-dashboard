import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form, Input, Button, Select, Upload, message } from "antd";
import ImgCrop from "antd-img-crop";
import { postCompany } from "../../redux/actions/staff.actions";
import { FaUser } from "react-icons/fa";
import { FiCamera } from "react-icons/fi";
import { HistoriesLoader } from "../Loaders";
import "./Form.scss";

const { Option } = Select;

export const AddCompany = () => {
  const dispatch = useDispatch();
  const [fileList, setFileList] = useState([{}]);
  const [loading, setLoading] = useState(false);
  const [companySize, setcompanySize] = useState("");
  const [image, setImage] = useState("");

  const {
    loading: postStaffLoading,
    successMessage,
    errorMessage,
  } = useSelector((state) => state.PostStaff);

  console.log(errorMessage);
  function onChange(value) {
    setcompanySize(value);
  }

  const onFinish = (values) => {
    dispatch(postCompany(values?.name, image, companySize));

    !postStaffLoading || successMessage !== null
      ? message.success(successMessage)
      : message.error(errorMessage);
  };

  function onSearch(val) {
    setcompanySize(val);
  }

  const onUploadChange = async ({ fileList: newFileList }) => {
    setFileList(newFileList);
    const data = new FormData();
    data.append("file", fileList[0]?.originFileObj);
    data.append("upload_preset", "show_app_images");
    setLoading(true);
    const res = await fetch(
      "	https://api.cloudinary.com/v1_1/dsq8qsg1a/image/upload",
      {
        method: "POST",
        body: data,
      }
    );

    const theFile = await res.json();
    setImage(theFile.secure_url);
    setLoading(false);
  };

  const onPreview = async (file) => {
    let src = file.url;
    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  return (
    <div className="lot_margin_1_top">
      <div className="lot_form">
        <ImgCrop rotate>
          <Upload
            listType="picture-card"
            // fileList={fileList}
            onChange={onUploadChange}
            onPreview={onPreview}
            className="show_uploader"
            showUploadList={false}
          >
            <div className="lot_upload_container">
              {loading && <HistoriesLoader />}
              {!loading && image ? (
                <img className="lot_image" src={image} alt="" />
              ) : (
                !loading && <FaUser className="icon" />
              )}
              <div className="camera">
                <FiCamera className="camera_icon" />
              </div>
            </div>
          </Upload>
        </ImgCrop>
        <Form name="addCompany" onFinish={onFinish}>
          <Form.Item
            name="name"
            rules={[
              {
                required: true,
                message: "Name",
              },
            ]}
          >
            <Input
              className="lot_input lot_bordered_input"
              placeholder="Name of the company"
            />
          </Form.Item>
          <Form.Item>
            <Select
              placeholder="Size of the campany"
              optionFilterProp="children"
              onChange={onChange}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              className="lot_input lot_bordered_input"
            >
              <Option value="SMALL">Small campany(10 - 100)</Option>
              <Option value="MEDIUM">Medium campany(100 - 1000)</Option>
              <Option value="LARGE">Large campany(1000+)</Option>
            </Select>
          </Form.Item>

          <Form.Item>
            {postStaffLoading ? (
              <Button
                disabled
                className="lot_margin_1_top lot_btn lot_large_btn lot_btn_primary_btn"
                type="primary"
                htmlType="submit"
              >
                Posting...
              </Button>
            ) : (
              <Button
                className="lot_margin_1_top lot_btn lot_large_btn lot_btn_primary_btn"
                type="primary"
                htmlType="submit"
              >
                Add
              </Button>
            )}
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};
