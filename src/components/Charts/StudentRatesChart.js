import React from "react";
import { HistoriesLoader } from "../Loaders";
import {
  XAxis,
  Tooltip,
  CartesianGrid,
  YAxis,
  Legend,
  BarChart,
  Bar,
} from "recharts";

export const StudentRatesChart = ({ companyData, companyLoading }) => {
  return (
    <div className="lot_card_container lot_extra_small_text">
      <p className="lot_margin_2_bottom lot_small_text lot_semiBold_text lot_margin_1_left">
        Rates
      </p>
      {companyLoading ? (
        HistoriesLoader
      ) : (
        <BarChart width={740} height={250} data={companyData}>
          <CartesianGrid stroke="#f5f5f5" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="rating" fill="#1B27AF" />
        </BarChart>
      )}
    </div>
  );
};
