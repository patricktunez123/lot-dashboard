import React from "react";
import {
  LineChart,
  XAxis,
  Tooltip,
  CartesianGrid,
  Line,
  YAxis,
  Legend,
} from "recharts";
import { HistoriesLoader } from "../Loaders";

export const OverviewChart = ({ companyData, companyLoading }) => {
  return (
    <div className="lot_card_container lot_extra_small_text">
      <p className="lot_margin_2_bottom lot_small_text lot_semiBold_text lot_margin_1_left">
        Success map
      </p>
      {companyLoading ? (
        HistoriesLoader
      ) : (
        <LineChart width={740} height={250} data={companyData}>
          <XAxis dataKey="name" />
          <YAxis />
          <CartesianGrid stroke="#f5f5f5" />
          <Tooltip />
          <Legend verticalAlign="top" height={36} />
          <Line type="monotone" dataKey="rating" stroke="#8884d8" />
          <Line type="monotone" dataKey="id" stroke="#82ca9d" />
        </LineChart>
      )}
    </div>
  );
};
