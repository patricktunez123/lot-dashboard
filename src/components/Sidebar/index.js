import React from "react";
import { Link, useHistory } from "react-router-dom";
import { IoIosArrowBack } from "react-icons/io";
import { webMenus } from "../Menus";
import profile_pic from "../../images/profile_pic.png";
import "./Sidebar.scss";

const Sidebar = () => {
  const history = useHistory();
  const handleToggle = () => {
    console.log("clicked");
  };

  const user = localStorage.getItem("lotUserData");
  const _user = JSON.parse(user);

  return (
    <div className="lot_sidebar">
      <div className="profile">
        <img
          src={
            _user?.profilePictureImgUrl || _user?.picture
              ? _user?.profilePictureImgUrl || _user?.picture
              : profile_pic
          }
          alt=""
        />
        <div className="profile_content">
          <p className="lot_extra_small_text lot_black_text lot_semiBold_text">
            {_user?.fullName || _user?.name}
          </p>
          <p className="lot_extra_small_text lot_muted_text"> {_user?.role}</p>
        </div>
      </div>

      <div className="lot_menus">
        {webMenus?.map((menu) => (
          <Link
            key={menu?.id}
            className={`lot_menu lot_secondary_text ${
              menu.url === history.location.pathname ? "active" : ""
            }`}
            to={menu?.url}
          >
            {menu?.icon}
            <span className="lot_small_text lot_medium_text">
              {menu?.title}
            </span>
          </Link>
        ))}
      </div>

      <div className="lot_side_toggler">
        <div className="toggler_menus" onClick={handleToggle}>
          <IoIosArrowBack className="toggle_icio" />
          <p className="lot_small_text">Hide</p>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
