import React from "react";
import { Link, useHistory } from "react-router-dom";
import { MdNavigateNext } from "react-icons/md";
import { webMenus } from "../Menus";
import profile_pic from "../../images/profile_pic.png";
import "./Sidebar.scss";

const ToggledSideBar = () => {
  const history = useHistory();
  const handleToggle = () => {
    console.log("clicked");
  };
  return (
    <div className="lot_min_sidebar">
      <div className="profile">
        <img src={profile_pic} alt="" />
      </div>

      <div className="lot_menus">
        {webMenus?.map((menu) => (
          <Link
            key={menu?.id}
            className={`lot_menu lot_secondary_text ${
              menu.url === history.location.pathname ? "active" : ""
            }`}
            to={menu?.url}
          >
            <img src={menu?.icon} alt="" />
          </Link>
        ))}
      </div>

      <div className="lot_side_toggler">
        <div className="toggler_menus" onClick={handleToggle}>
          <MdNavigateNext className="toggle_icio" />
        </div>
      </div>
    </div>
  );
};

export default ToggledSideBar;
