import axios from "axios";

const request = axios.create({
  baseURL: "https://lot.awesomity.rw/api",
});

export default request;
