import * as Yup from "yup";

export const name = Yup.string()
  .required("Name must be provided")
  .min(2, "That's a fanny looking name")
  .max(40, "The name is too long!");

export const email = Yup.string()
  .email("Invalid email")
  .required("No email provided");

export const password = Yup.string()
  .required("No password provided.")
  .min(4, "Password is too short, Atleast 4 chars")
  .max(20, "Password is too long");
