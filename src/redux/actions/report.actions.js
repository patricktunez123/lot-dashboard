import { db } from "../../config/firebase";
import { reportActionTypes } from "../action-types";

export const getAllReports = () => async (dispatch) => {
  try {
    dispatch({
      type: reportActionTypes.GET_ALL_REPORTS_REQUEST,
    });

    await db.collection("reports").onSnapshot((snapshot) => {
      const data = snapshot.docs.map((doc) => ({
        id: doc.id,
        company: doc.data()?.company,
        dateJoined: doc.data()?.dateJoined,
        key: doc.data()?.key,
        serviceFee: doc.data()?.serviceFee,
        studentEmail: doc.data()?.studentEmail,
        studentName: doc.data()?.studentName,
      }));

      dispatch({
        type: reportActionTypes.GET_ALL_REPORTS_SUCCESS,
        payload: data,
      });
    });
  } catch (error) {
    dispatch({
      type: reportActionTypes.GET_ALL_REPORTS_FAIL,
      payload: error,
    });
  }
};
