import firebase from "firebase";
import apiRequest from "../../helpers/api";
import { auth } from "../../config/firebase";
import { authActionTypes } from "../action-types";
import { routes } from "../../config/route-config";

export const login = (data) => async (dispatch) => {
  try {
    dispatch({
      type: authActionTypes.LOGIN_REQUEST,
    });

    const res = await apiRequest.post("/v1/auth/login", {
      email: data?.email,
      password: data?.password,
      headers: {
        "Content-Type": "application/json",
      },
    });

    localStorage.setItem("lotUserToken", res?.data?.payload?.accessToken);
    localStorage.setItem(
      "lotUserData",
      JSON.stringify(res?.data?.payload?.user)
    );

    dispatch({
      type: authActionTypes.LOGIN_SUCCESS,
      payload: res,
    });
  } catch (error) {
    dispatch({
      type: authActionTypes.LOGIN_FAIL,
      payload: error.message,
    });
  }
};

export const userRegister = (data) => async (dispatch) => {
  try {
    dispatch({
      type: authActionTypes.USER_REGISTER_REQUEST,
    });

    const res = await apiRequest.post("/v1/auth/sign-up", {
      fullName: data?.name,
      email: data?.email,
      password: data?.password,
      headers: {
        "Content-Type": "application/json",
      },
    });

    localStorage.setItem("lotUserToken", res?.data?.payload?.accessToken);
    localStorage.setItem(
      "lotUserData",
      JSON.stringify(res?.data?.payload?.user)
    );

    dispatch({
      type: authActionTypes.USER_REGISTER_SUCCESS,
      payload: res?.data?.payload,
    });
  } catch (error) {
    dispatch({
      type: authActionTypes.USER_REGISTER_FAIL,
      payload: error.message,
    });
  }
};

export const registerWithGoogle = () => async (dispatch) => {
  try {
    dispatch({
      type: authActionTypes.REGISTER_WITH_GOOGLE_REQUEST,
    });

    const googleProvider = new firebase.auth.GoogleAuthProvider();
    const response = await auth.signInWithPopup(googleProvider);
    localStorage.setItem("lotUserToken", response?.credential?.accessToken);
    localStorage.setItem(
      "lotUserData",
      JSON.stringify(response?.additionalUserInfo?.profile)
    );
    window.location.assign(routes.overview.url);
    console.log("====this is the res", response);

    await apiRequest.post("/v1/auth/sign-up/third-party", {
      fullName: response?.additionalUserInfo?.profile?.name,
      email: response?.additionalUserInfo?.profile?.email,
      headers: {
        "Content-Type": "application/json",
      },
    });

    dispatch({
      type: authActionTypes.REGISTER_WITH_GOOGLE_SUCCESS,
      payload: response?.additionalUserInfo?.profile,
    });
  } catch (error) {
    dispatch({
      type: authActionTypes.REGISTER_WITH_GOOGLE_FAIL,
      payload: error,
    });
  }
};
