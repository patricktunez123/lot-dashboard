import apiRequest from "../../helpers/api";
import { staffActionTypes } from "../action-types";
const userToken = localStorage.getItem("lotUserToken");

export const getAllStaffs = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: staffActionTypes.GET_ALL_STAFFS_REQUEST,
    });
    const limit = getState()?.CompaniesCount?.count;
    const companies = await apiRequest.get(`/v1/companies/?limit=${limit}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    });
    dispatch({
      type: staffActionTypes.GET_ALL_STAFFS_SUCCESS,
      payload: companies?.data?.payload?.data,
    });
  } catch (error) {
    dispatch({
      type: staffActionTypes.GET_ALL_STAFFS_FAIL,
      payload: error,
    });
  }
};

export const getCompany = (id) => async (dispatch) => {
  try {
    dispatch({
      type: staffActionTypes.GET_COMPANY_REQUEST,
    });

    const res = await apiRequest.get(`/v1/companies/${id}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    });

    dispatch({
      type: staffActionTypes.GET_COMPANY_SUCCESS,
      payload: res,
    });
  } catch (error) {
    dispatch({
      type: staffActionTypes.GET_COMPANY_FAIL,
      payload: error,
    });
  }
};

export const getCompaniesCount = () => async (dispatch) => {
  try {
    dispatch({
      type: staffActionTypes.GET_COMPANIES_COUNT_REQUEST,
    });

    const companiesCount = await apiRequest.get("/v1/companies/count", {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    });
    dispatch({
      type: staffActionTypes.GET_COMPANIES_COUNT_SUCCESS,
      payload: companiesCount?.data?.payload?.count,
    });
  } catch (error) {
    dispatch({
      type: staffActionTypes.GET_COMPANIES_COUNT_FAIL,
      payload: error,
    });
  }
};

export const postCompany = (name, picture, size) => async (dispatch) => {
  try {
    dispatch({
      type: staffActionTypes.POST_STAFF_REQUEST,
    });

    const res = await apiRequest.post("/v1/companies", {
      name: name,
      picture: picture,
      size: size,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("lotUserToken")}`,
      },
    });

    dispatch({
      type: staffActionTypes.POST_STAFF_SUCCESS,
      payload: res,
    });
  } catch (error) {
    dispatch({
      type: staffActionTypes.POST_STAFF_FAIL,
      payload: error,
    });
  }
};

export const deleteCompany = (id) => async (dispatch) => {
  try {
    dispatch({
      type: staffActionTypes.DELETE_COMPANY_REQUEST,
    });

    const res = await apiRequest.delete(`/v1/companies/${id}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    });

    dispatch({
      type: staffActionTypes.DELETE_COMPANY_SUCCESS,
      payload: res,
    });
  } catch (error) {
    dispatch({
      type: staffActionTypes.DELETE_COMPANY_FAIL,
      payload: error,
    });
  }
};

export const rateCompany = (id, value) => async (dispatch) => {
  try {
    dispatch({
      type: staffActionTypes.RATE_COMPANY_REQUEST,
    });

    const res = await apiRequest.put(`/v1/companies/${id}/rate`, {
      rating: value,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    });

    dispatch({
      type: staffActionTypes.RATE_COMPANY_SUCCESS,
      payload: res,
    });
  } catch (error) {
    dispatch({
      type: staffActionTypes.RATE_COMPANY_FAIL,
      payload: error,
    });
  }
};
