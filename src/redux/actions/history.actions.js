import apiRequest from "../../helpers/api";
import { historyActionTypes } from "../action-types";
const userToken = localStorage.getItem("lotUserToken");

export const getAllHistories = () => async (dispatch) => {
  try {
    dispatch({
      type: historyActionTypes.GET_ALL_HISTORIES_REQUEST,
    });

    const events = await apiRequest.get("/v1/events", {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    });
    dispatch({
      type: historyActionTypes.GET_ALL_HISTORIES_SUCCESS,
      payload: events?.data?.payload?.data,
    });
  } catch (error) {
    dispatch({
      type: historyActionTypes.GET_ALL_HISTORIES_FAIL,
      payload: error,
    });
  }
};
