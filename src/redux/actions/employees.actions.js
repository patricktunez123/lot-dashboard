import apiRequest from "../../helpers/api";
import { employeesActionTypes } from "../action-types";
const userToken = localStorage.getItem("lotUserToken");

export const getAllEmployees = () => async (dispatch) => {
  try {
    dispatch({
      type: employeesActionTypes.GET_ALL_EMPLOYEES_REQUEST,
    });

    const employees = await apiRequest.get("/v2/employees?status=ALL", {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    });
    dispatch({
      type: employeesActionTypes.GET_ALL_EMPLOYEES_SUCCESS,
      payload: employees?.data?.payload?.data,
    });
  } catch (error) {
    dispatch({
      type: employeesActionTypes.GET_ALL_EMPLOYEES_FAIL,
      payload: error,
    });
  }
};

export const getEmployessCount = () => async (dispatch) => {
  try {
    dispatch({
      type: employeesActionTypes.GET_EMPLOYEES_COUNT_REQUEST,
    });

    const employeesCount = await apiRequest.get("/v2/employees/count/all", {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    });
    dispatch({
      type: employeesActionTypes.GET_EMPLOYEES_COUNT_SUCCESS,
      payload: employeesCount?.data?.payload,
    });
  } catch (error) {
    dispatch({
      type: employeesActionTypes.GET_EMPLOYEES_COUNT_FAIL,
      payload: error,
    });
  }
};

export const deleteEmployee = (id) => async (dispatch) => {
  try {
    dispatch({
      type: employeesActionTypes.DELETE_EMPLOYEE_REQUEST,
    });

    const res = await apiRequest.delete(`/v2/employees/${id}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    });

    dispatch({
      type: employeesActionTypes.DELETE_EMPLOYEE_SUCCESS,
      payload: res,
    });
  } catch (error) {
    dispatch({
      type: employeesActionTypes.DELETE_EMPLOYEE_FAIL,
      payload: error,
    });
  }
};

export const postEmployee =
  (firstName, lastName, email, picture) => async (dispatch) => {
    try {
      dispatch({
        type: employeesActionTypes.POST_EMPLOYEE_REQUEST,
      });

      const res = await apiRequest.post("/v2/employees", {
        firstName: firstName,
        lastName: lastName,
        email: email,
        picture: picture,
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("lotUserToken")}`,
        },
      });

      dispatch({
        type: employeesActionTypes.POST_EMPLOYEE_SUCCESS,
        payload: res,
      });
    } catch (error) {
      dispatch({
        type: employeesActionTypes.POST_EMPLOYEE_FAIL,
        payload: error,
      });
    }
  };
