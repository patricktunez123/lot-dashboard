import { reportActionTypes } from "../action-types";

const initialState = {
  loading: false,
  errorMessage: null,
  successMessage: null,
  data: [],
};

export const getAllReports = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case reportActionTypes.GET_ALL_REPORTS_REQUEST:
      return {
        ...prevState,
        loading: true,
      };
    case reportActionTypes.GET_ALL_REPORTS_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };
    case reportActionTypes.GET_ALL_REPORTS_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};
