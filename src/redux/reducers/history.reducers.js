import { historyActionTypes } from "../action-types";

const initialState = {
  loading: false,
  errorMessage: null,
  successMessage: null,
  data: [],
};

export const getAllHistories = (
  prevState = initialState,
  { type, payload }
) => {
  switch (type) {
    case historyActionTypes.GET_ALL_HISTORIES_REQUEST:
      return {
        ...prevState,
        loading: true,
      };
    case historyActionTypes.GET_ALL_HISTORIES_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };
    case historyActionTypes.GET_ALL_HISTORIES_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};
