import { combineReducers } from "redux";
import { getAllHistories } from "./history.reducers";
import {
  getAllStaffs,
  getCompany,
  getCompaniesCount,
  postStaff,
  deleteCompany,
  rateCompany,
} from "./staff.reducers";
import { getAllReports } from "./report.reducers";
import {
  getAllEmployees,
  deleteEmployee,
  postEmployee,
  getEmployeesCount,
} from "./employees.reducers";
import { login, userRegister } from "./auth.reducers";

export const reducers = combineReducers({
  Histories: getAllHistories,
  Staffs: getAllStaffs,
  Employees: getAllEmployees,
  EmployeesCount: getEmployeesCount,
  DeleteEmployee: deleteEmployee,
  PostEmployee: postEmployee,
  Company: getCompany,
  CompaniesCount: getCompaniesCount,
  RateCompany: rateCompany,
  PostStaff: postStaff,
  DeleteCompany: deleteCompany,
  Reports: getAllReports,
  Login: login,
  UserRegister: userRegister,
});
