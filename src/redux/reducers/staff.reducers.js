import { staffActionTypes } from "../action-types";

const initialState = {
  loading: false,
  errorMessage: null,
  successMessage: null,
  data: [],
  count: null,
};

export const getAllStaffs = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case staffActionTypes.GET_ALL_STAFFS_REQUEST:
      return {
        ...prevState,
        loading: true,
      };
    case staffActionTypes.GET_ALL_STAFFS_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };
    case staffActionTypes.GET_ALL_STAFFS_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const getCompany = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case staffActionTypes.GET_COMPANY_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case staffActionTypes.GET_COMPANY_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };

    case staffActionTypes.GET_COMPANY_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const getCompaniesCount = (
  prevState = initialState,
  { type, payload }
) => {
  switch (type) {
    case staffActionTypes.GET_COMPANIES_COUNT_REQUEST:
      return {
        ...prevState,
        loading: true,
      };
    case staffActionTypes.GET_COMPANIES_COUNT_SUCCESS:
      return {
        ...prevState,
        loading: false,
        count: payload,
      };
    case staffActionTypes.GET_COMPANIES_COUNT_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const postStaff = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case staffActionTypes.POST_STAFF_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case staffActionTypes.POST_STAFF_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };

    case staffActionTypes.POST_STAFF_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const deleteCompany = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case staffActionTypes.DELETE_COMPANY_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case staffActionTypes.DELETE_COMPANY_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };

    case staffActionTypes.DELETE_COMPANY_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const rateCompany = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case staffActionTypes.RATE_COMPANY_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case staffActionTypes.RATE_COMPANY_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };

    case staffActionTypes.RATE_COMPANY_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};
