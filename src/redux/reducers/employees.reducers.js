import { employeesActionTypes } from "../action-types";

const initialState = {
  loading: false,
  errorMessage: null,
  successMessage: null,
  data: [],
  count: null,
};

export const getAllEmployees = (
  prevState = initialState,
  { type, payload }
) => {
  switch (type) {
    case employeesActionTypes.GET_ALL_EMPLOYEES_REQUEST:
      return {
        ...prevState,
        loading: true,
      };
    case employeesActionTypes.GET_ALL_EMPLOYEES_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };
    case employeesActionTypes.GET_ALL_EMPLOYEES_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const deleteEmployee = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case employeesActionTypes.DELETE_EMPLOYEE_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case employeesActionTypes.DELETE_EMPLOYEE_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };

    case employeesActionTypes.DELETE_EMPLOYEE_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const postEmployee = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case employeesActionTypes.POST_EMPLOYEE_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case employeesActionTypes.POST_EMPLOYEE_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };

    case employeesActionTypes.POST_EMPLOYEE_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const getEmployeesCount = (
  prevState = initialState,
  { type, payload }
) => {
  switch (type) {
    case employeesActionTypes.GET_EMPLOYEES_COUNT_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case employeesActionTypes.GET_EMPLOYEES_COUNT_SUCCESS:
      return {
        ...prevState,
        loading: false,
        count: payload,
      };

    case employeesActionTypes.GET_EMPLOYEES_COUNT_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};
