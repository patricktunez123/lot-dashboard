import { authActionTypes } from "../action-types";

const initialState = {
  loading: false,
  errorMessage: null,
  successMessage: null,
  data: [],
  count: null,
};

export const login = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case authActionTypes.LOGIN_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case authActionTypes.LOGIN_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };

    case authActionTypes.LOGIN_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};

export const userRegister = (prevState = initialState, { type, payload }) => {
  switch (type) {
    case authActionTypes.USER_REGISTER_REQUEST:
      return {
        ...prevState,
        loading: true,
      };

    case authActionTypes.USER_REGISTER_SUCCESS:
      return {
        ...prevState,
        loading: false,
        data: payload,
      };

    case authActionTypes.USER_REGISTER_FAIL:
      return {
        ...prevState,
        loading: false,
        errorMessage: payload,
      };

    default:
      return prevState;
  }
};
