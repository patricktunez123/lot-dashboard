import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./Overview.scss";
import {
  getCompaniesCount,
  getAllStaffs,
} from "../../redux/actions/staff.actions";
import { getEmployessCount } from "../../redux/actions/employees.actions";
import {
  CompaniesOverViewCard,
  EmployeesOverViewCard,
  EventsOverViewCard,
} from "../../components/Cards";
import { getAllHistories } from "../../redux/actions/history.actions";
import { OverviewChart } from "../../components/Charts";
import { StudentRatesChart } from "../../components/Charts";

const Overview = () => {
  const dispatch = useDispatch();

  const { count, loading: countLoading } = useSelector(
    (state) => state.CompaniesCount
  );

  const { count: empCount, loading: empCountLoading } = useSelector(
    (state) => state.EmployeesCount
  );

  const { data: histories, loading: eventsLoading } = useSelector(
    (state) => state.Histories
  );

  const { data: companyData, loading: companyLoading } = useSelector(
    (state) => state.Staffs
  );
  useEffect(() => {
    dispatch(getCompaniesCount());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getEmployessCount());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getAllHistories());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getAllStaffs());
  }, [dispatch]);

  return (
    <div className="lot_content">
      <div className="lot_sticky_header">
        <h6 className="lot_medium_title">
          Welcome to <span className="lot_semiBold_text">Lot,</span>
        </h6>
      </div>
      <div className="lot_content_container lot_padding_2">
        <OverviewChart
          companyData={companyData}
          companyLoading={companyLoading}
        />

        <div className="overview_cards lot_margin_3_top lot_margin_3_bottom">
          <CompaniesOverViewCard countLoading={countLoading} count={count} />
          <EmployeesOverViewCard
            countLoading={empCountLoading}
            count={empCount}
          />
          <EventsOverViewCard
            countLoading={eventsLoading}
            count={histories?.length}
          />
        </div>

        <StudentRatesChart
          companyData={companyData}
          companyLoading={companyLoading}
        />
      </div>
    </div>
  );
};

export default Overview;
