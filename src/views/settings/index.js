import React from "react";
import "./Settings.scss";
import maintenance from "../../images/maintenance.svg";

const Settings = () => {
  return (
    <div className="lot_content">
      <div className="lot_sticky_header">
        {/* <h6 className="lot_medium_title">Settings</h6> */}
      </div>
      <div className="lot_content_container lot_padding_2">
        <div className="not_done_page">
          <img src={maintenance} alt="" />
          <h6 className="lot_primary_text lot_small_title">
            Under constraction page!
          </h6>
        </div>
      </div>
    </div>
  );
};

export default Settings;
