import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal } from "antd";
import "./Staffs.scss";
import { StaffsTable, EmployeesTable } from "../../components/Tables";
import { AddCompany } from "../../components/Forms";
import {
  getAllStaffs,
  getCompaniesCount,
} from "../../redux/actions/staff.actions";

const Staffs = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedMenu, setSelectedMenu] = useState("Companies");

  const dispatch = useDispatch();
  const { data: staffs, loading } = useSelector((state) => state.Staffs);
  const { count, loading: countLoading } = useSelector(
    (state) => state.CompaniesCount
  );

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  useEffect(() => {
    dispatch(getAllStaffs());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getCompaniesCount());
  }, [dispatch]);

  const onCampaniesChange = () => {
    setSelectedMenu("Companies");
  };

  const onEmpChange = () => {
    setSelectedMenu("Employers");
  };

  return (
    <div className="lot_content">
      <div className="lot_sticky_header">
        <div className="lot_tabs lot_tabs_with_bg">
          <Button
            onClick={onCampaniesChange}
            className={`lot_link_btn ${
              selectedMenu === " Companies" ? "active_menu" : "default_text"
            }`}
          >
            Companies
          </Button>
          <Button
            onClick={onEmpChange}
            className={`lot_link_btn ${
              selectedMenu === "Employers" ? "active_menu" : "default_text"
            }`}
          >
            Employers
          </Button>
        </div>
        <Button onClick={showModal} className="lot_btn lot_btn_primary_btn">
          Add
        </Button>
        <Modal
          title="Add Company"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          footer={false}
          closable={false}
          className="lot_modal"
        >
          <AddCompany />
        </Modal>
      </div>
      <div className="lot_content_container">
        {selectedMenu === "Companies" ? (
          <StaffsTable
            loading={loading}
            staffs={staffs}
            countLoading={countLoading}
            count={count}
          />
        ) : (
          <EmployeesTable
            loading={loading}
            staffs={staffs}
            countLoading={countLoading}
            count={count}
          />
        )}
      </div>
    </div>
  );
};

export default Staffs;
