import React from "react";
import { Redirect } from "react-router-dom";
import logo from "../../images/white_logo.svg";
import "../../styles/authPages.scss";
import { LoginForm } from "../../components/Forms";
import { routes } from "../../config/route-config";

const Login = () => {
  const userToken = localStorage.getItem("lotUserToken");
  return (
    <>
      {userToken ? (
        <Redirect to={routes.overview.url} />
      ) : (
        <div className="lot_container">
          <div className="lot_auth_container">
            <div className="lot_banner">
              <img src={logo} alt="" />
              <p className="lot_medium_title lot_white_text lot_italic_text">
                “Let the countdown begin”
              </p>
            </div>
            <div className="lot_form_container">
              <div className="form_content">
                <div className="lot_form_header">
                  <p className="lot_medium_text lot_primary_text lot_bold_text lot_margin_1_bottom">
                    Log In
                  </p>
                  <p className="lot_grey_text lot_margin_1_bottom">
                    We’re glad you are back! Now fill in the details And dive in
                  </p>
                </div>
                <LoginForm />
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Login;
