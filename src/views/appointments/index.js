import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import "./Appointments.scss";
import SingleHistory from "../../components/LotHistory/SingleHistory";
import { getAllHistories } from "../../redux/actions/history.actions";
import { HistoriesLoader } from "../../components/Loaders";

const Appointments = () => {
  const [value, onChange] = useState(new Date());
  const dispatch = useDispatch();
  const { data: histories, loading } = useSelector((state) => state.Histories);

  useEffect(() => {
    dispatch(getAllHistories());
  }, [dispatch]);

  return (
    <div className="lot_content">
      <div className="lot_sticky_header">
        <h6>Calender</h6>
      </div>
      <div className="lot_content_container lot_padding_2">
        <div className="lot_card_container lot_card_heigt_50 lot_extra_small_text">
          <Calendar
            className="lot_calendar"
            onChange={onChange}
            value={value}
          />
        </div>
        <h6 className="lot_margin_2_top lot_margin_2_bottom">
          June main Events
        </h6>
        <div className="row">
          {loading ? (
            <HistoriesLoader />
          ) : (
            histories?.slice(0, 3)?.map((history) => {
              return (
                <div className="col-md-4 col-lg-4 col-12">
                  <div className="lot_histories_container lot_margin_2_top">
                    <SingleHistory key={history?.id} {...history} />
                  </div>
                </div>
              );
            })
          )}
        </div>
      </div>
    </div>
  );
};

export default Appointments;
