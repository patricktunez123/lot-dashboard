import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DatePicker } from "antd";
import { RiArrowDropDownLine } from "react-icons/ri";
import "./Reports.scss";
import { ReportsTable } from "../../components/Tables";
import { getAllReports } from "../../redux/actions/report.actions";

const Reports = () => {
  const dispatch = useDispatch();
  const { data: reports, loading } = useSelector((state) => state.Reports);

  function onChange(date, dateString) {
    console.log(date, dateString);
  }
  useEffect(() => {
    dispatch(getAllReports());
  }, [dispatch]);

  return (
    <div className="lot_content">
      <div className="lot_sticky_header">
        <h6>Students rate</h6>
        <div className="lot_tabs">
          <span className="lot_primary_text lot_small_text lot_semiBold_text">
            From
          </span>{" "}
          <DatePicker
            placeholder="1 Jan"
            className="lot_input lot_bordered_input"
            onChange={onChange}
            suffixIcon={<RiArrowDropDownLine />}
          />
          <span className="lot_primary_text lot_small_text lot_semiBold_text">
            To
          </span>
          <DatePicker
            className="lot_input lot_bordered_input"
            placeholder="31 Mar"
            onChange={onChange}
            suffixIcon={<RiArrowDropDownLine />}
          />
        </div>
      </div>
      <div className="lot_content_container lot_padding_2">
        <ReportsTable loading={loading} reports={reports} />
      </div>
    </div>
  );
};

export default Reports;
