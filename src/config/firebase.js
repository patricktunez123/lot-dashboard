import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyDsTih72OHHSv2b-Sc4kgrHx4tPN9lpd-E",
  authDomain: "the-lot-app.firebaseapp.com",
  databaseURL: "https://the-lot-app.firebaseio.com/",
  projectId: "the-lot-app",
  storageBucket: "the-lot-app.appspot.com",
  messagingSenderId: "1037857567439",
  appId: "1:1037857567439:web:b5d0d88ebce9f8fc3a3dba",
  measurementId: "G-2G267XBN7P",
};

let app = null;

if (firebase.apps.length === 0) {
  app = firebase.initializeApp(firebaseConfig);
} else {
  app = firebase.app();
}

export const db = app.firestore();
export const auth = app.auth();
export const storage = app.storage();
