export const routes = {
  login: {
    name: "Login",
    url: "/",
  },
  register: {
    name: "Register",
    url: "/create-account",
  },
  overview: {
    name: "Overview",
    url: "/overview",
  },
  appointments: {
    name: "Appointments",
    url: "/appointments",
  },
  staffs: {
    name: "Staffs",
    url: "/staffs",
  },
  reports: {
    name: "Reports",
    url: "/reports",
  },
  settings: {
    name: "Settings",
    url: "/settings",
  },
};
