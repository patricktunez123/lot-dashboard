import React, { Fragment, Suspense } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { Spin } from "antd";
import { routes } from "../../config/route-config";
import Layout from "../../components/Layout";

const Overview = React.lazy(() => import("../../views/overview"));
const Appointments = React.lazy(() => import("../../views/appointments"));
const Staffs = React.lazy(() => import("../../views/staffs"));
const Reports = React.lazy(() => import("../../views/reports"));
const Settings = React.lazy(() => import("../../views/settings"));

const PrivateRoutes = () => {
  const userToken = localStorage.getItem("lotUserToken");
  if (!userToken) return <Redirect to={routes.login.url} />;
  return (
    <Fragment>
      <Suspense
        fallback={
          <>
            {userToken ? (
              <Layout>
                <div
                  style={{
                    height: "70vh",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Spin />
                </div>
              </Layout>
            ) : (
              <div
                style={{
                  height: "70vh",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Spin />
              </div>
            )}
          </>
        }
      >
        <Switch>
          <Route path={routes.overview.url} exact>
            <Layout>
              <Overview />
            </Layout>
          </Route>

          <Route path={routes.appointments.url} exact>
            <Layout>
              <Appointments />
            </Layout>
          </Route>

          <Route path={routes.staffs.url} exact>
            <Layout>
              <Staffs />
            </Layout>
          </Route>

          <Route path={routes.reports.url} exact>
            <Layout>
              <Reports />
            </Layout>
          </Route>

          <Route path={routes.settings.url} exact>
            <Layout>
              <Settings />
            </Layout>
          </Route>
        </Switch>
      </Suspense>
    </Fragment>
  );
};

export default PrivateRoutes;
