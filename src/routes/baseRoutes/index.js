import React, { Fragment, Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import { Spin } from "antd";
import { routes } from "../../config/route-config";
import Layout from "../../components/Layout";
import Login from "../../views/login";
import Register from "../../views/register";
import Error404 from "../../views/error404";

const PrivateRoutes = React.lazy(() => import("../privateRoutes"));

const Routes = () => {
  const userToken = localStorage.getItem("lotUserToken");
  return (
    <Fragment>
      <Suspense
        fallback={
          <>
            {userToken ? (
              <Layout>
                <div
                  style={{
                    height: "70vh",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Spin />
                </div>
              </Layout>
            ) : (
              <div
                style={{
                  height: "70vh",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Spin />
              </div>
            )}
          </>
        }
      >
        <Switch>
          <Route path={routes.login.url} exact>
            <Login />
          </Route>

          <Route path={routes.register.url} exact>
            <Register />
          </Route>

          <Route component={PrivateRoutes} />
          <Route>
            <Error404 />
          </Route>
        </Switch>
      </Suspense>
    </Fragment>
  );
};

export default Routes;
